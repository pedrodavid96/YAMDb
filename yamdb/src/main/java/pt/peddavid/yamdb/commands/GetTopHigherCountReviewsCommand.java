package pt.peddavid.yamdb.commands;

import kotlin.Pair;
import pt.peddavid.yamdb.libs.command.CommandArguments;
import pt.peddavid.yamdb.libs.database.ConnectionService;
import pt.peddavid.yamdb.model.Movie;
import pt.peddavid.yamdb.util.RsMappers;

import java.sql.SQLException;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class GetTopHigherCountReviewsCommand implements SQLCommand<List<Pair<Movie, Long>>> {

    private final ConnectionService src;

    public GetTopHigherCountReviewsCommand(ConnectionService src) {
        this.src = src;
    }

    @Override
    public List<Pair<Movie, Long>> executeSql(CommandArguments args) throws CommandArguments.InvalidArgumentException, SQLException {
        int n = args.demandIntVariable("{n}");

        return src.query(
                "select Movie.id, Movie.Title, Movie.ReleaseYear, Movie.AddedDate, count(Rev.MovieId) as Count " +
                    "from Movie left outer join (select MovieID from dbo.Review) Rev on (Rev.MovieId = Movie.Id) " +
                "group by Movie.id, Movie.Title, Movie.ReleaseYear, Movie.AddedDate order by Count desc, id asc")
                .map(RsMappers::MovieWithCount)
                .setMaxRows(n)
                .collect(toList());
    }

    @Override
    public String toString() {
        return "GetTopHigherCountReviewsCommand - returns the detail for the movie with most reviews";
    }
}
