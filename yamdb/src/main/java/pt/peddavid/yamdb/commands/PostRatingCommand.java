package pt.peddavid.yamdb.commands;

import pt.peddavid.yamdb.libs.command.CommandArguments;
import pt.peddavid.yamdb.libs.database.ConnectionService;

import java.sql.SQLException;

public class PostRatingCommand implements SQLCommand<Class<Void>> {

    private final ConnectionService src;

    public PostRatingCommand(ConnectionService src) {
        this.src = src;
    }

    @Override
    public Class<Void> executeSql(CommandArguments args) throws CommandArguments.InvalidArgumentException, SQLException {
        int rating = args.demandIntParameter("rating");
        int mid = args.demandIntVariable("{mid}");

        src.update("insert into Rating(Score, MovieId) values (?, ?)")
                .setInt(1, rating)
                .setInt(2, mid)
                .fetchGeneratedKeys();

        return Void.TYPE;
    }

    @Override
    public String toString() {
        return "PostRatingCommand - submits a new rating for the movie identified by {mid}, given the following parameters\n" +
                "\trating - number between 1 and 5.";
    }
}
