package pt.peddavid.yamdb.commands;

import kotlin.Pair;
import pt.peddavid.yamdb.libs.command.CommandArguments;
import pt.peddavid.yamdb.libs.command.CommandException;
import pt.peddavid.yamdb.libs.database.ConnectionService;
import pt.peddavid.yamdb.model.Movie;
import pt.peddavid.yamdb.util.Page;
import pt.peddavid.yamdb.util.RsMappers;

import java.sql.SQLException;
import java.util.Optional;

public class GetAllMoviesCommand implements SQLCommand<Page<Pair<Movie, Double>>>, Pageable {

    private final ConnectionService src;

    public GetAllMoviesCommand(ConnectionService src) {
        this.src = src;
    }

    @Override
    public Page<Pair<Movie, Double>> executeSql(CommandArguments args) throws SQLException, CommandException {
        Optional<Integer> skip = args.optionalIntParameter("skip");
        Optional<Integer> top = args.optionalIntParameter("top");

        return src.query(
                "select id, Title, ReleaseYear, AddedDate, avg from dbo.Movie " +
                "left outer join (select MovieId, avg(cast(score as Float)) as avg " +
                    "from dbo.Rating group by MovieId) as rating on (id = rating.MovieId)")
                .map(RsMappers::MovieWithAverage)
                .sort(sanitize(args.optionalParameter("sortBy")))
                .skip(skip.orElse(0))
                .top(top.map((value) -> value + 1).orElse(Integer.MAX_VALUE)) //map to + 1 so it cab check if it has more
                .collect(Page.getCollector(skip, top));
    }

    private String sanitize(Optional<String> opt) throws SQLException {
        if(!opt.isPresent()){
            return "(select 1)";
        }
        switch (opt.get()){
            case "addedDate" : return "addedDate ASC";
            case "addedDateDesc" : return "addedDate DESC";
            case "year" : return "releaseYear ASC";
            case "yearDesc" : return "releaseYear DESC";
            case "title" : return "Title ASC";
            case "titleDesc" : return "Title DESC";
            case "rating" : return "avg ASC";
            case "ratingDesc": return "avg DESC";
            default:
                throw new SQLException("Parameter sortBy used with Invalid criteria");
        }
    }

    @Override
    public String toString() {
        return "GetAllMoviesCommand - Returns a list fromCollection all movies in Database";
    }
}
