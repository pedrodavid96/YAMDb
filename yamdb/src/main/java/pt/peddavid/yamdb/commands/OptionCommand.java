package pt.peddavid.yamdb.commands;

import pt.peddavid.yamdb.libs.command.Command;
import pt.peddavid.yamdb.libs.command.CommandArguments;
import pt.peddavid.yamdb.libs.command.CommandManager;

public class OptionCommand implements Command<String> {

    private final CommandManager cmdManager;

    public OptionCommand(CommandManager manager){
        this.cmdManager = manager;
    }

    @Override
    public String execute(CommandArguments args) {
        String toRet = cmdManager.toString();
        System.out.println(toRet);
        return toRet;
    }

    @Override
    public String toString(){
        return "Presents a list fromCollection available commands and their characteristics";
    }
}
