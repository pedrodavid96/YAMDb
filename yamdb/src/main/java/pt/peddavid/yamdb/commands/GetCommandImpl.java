package pt.peddavid.yamdb.commands;

import pt.peddavid.commons.Formatter;
import pt.peddavid.yamdb.libs.command.Command;
import pt.peddavid.yamdb.libs.command.CommandArguments;
import pt.peddavid.yamdb.libs.command.CommandException;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Optional;
import java.util.function.Supplier;

public class GetCommandImpl<E> implements GetCommand<E> {

    private static final int FILE_WRITE_ERROR_CODE = 500;

    private final Command<E> command;
    private final Formatter<E> formatter;

    public GetCommandImpl(Command<E> cmd, Formatter<E> formatter){
        this.command = cmd;
        this.formatter = formatter;
    }

    @Override
    public E get(CommandArguments args) throws CommandException {
        return command.execute(args);
    }

    @Override
    public String format(E resource, CommandArguments args) {
        Supplier<String> format = () -> formatter.format(resource);
        return args.optionalHeader("accept")
                .map((arg) -> arg.equals("text/plain")  ? resource.toString() : format.get())
                .orElseGet(format);
    }

    @Override
    public void display(String formatted, CommandArguments args) throws CommandException{
        Optional<String> fileName = args.optionalHeader("file-name");
        if(fileName.isPresent()){
            try (FileWriter fw = new FileWriter(fileName.get())) {
                fw.write(formatted);
            } catch (IOException e) {
                throw new CommandException(FILE_WRITE_ERROR_CODE, "Error in file: " + fileName);
            }
        } else {
            System.out.println(formatted);
        }
    }

    public Command<E> getCommand(){
        return command;
    }

    @Override
    public String toString(){
        return command.toString();
    }
}
