package pt.peddavid.yamdb;

import pt.peddavid.yamdb.libs.database.ConnectionService;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class InsertScript {

    public static void main(String[] args) throws IOException, SQLException {
        ConnectionService service = new MSSQLService();
        service.transaction((trans) ->
                trans.batch("insert into dbo.Movie (Title,ReleaseYear) values (?, ?)")
                    .setString(1, "The Shawshank Redemption").setInt(2, 1994)
                    .addBatch()
                    .setString(1, "The Godfather").setInt(2, 1972)
                    .addBatch()
                    .setString(1, "The Dark Knight").setInt(2, 2008)
                    .addBatch()
                    .setString(1, "Schindler's List").setInt(2, 1993)
                    .addBatch()
                    .setString(1, "The Lord of the Rings: The Return of the King").setInt(2, 2003)
                    .addBatch()
                    .setString(1, "Star Wars: Episode V - The Empire Strikes Back").setInt(2, 1980)
                    .addBatch()
                    .setString(1, "Forrest Gump").setInt(2, 1994)
                    .addBatch()
                    .execute()
        );

        service.transaction((trans)-> {
            trans.batch("insert into dbo.Rating(Score,MovieId) values (?, ?)")
                    .setInt(1, 2).setInt(2, 3)
                    .addBatch()
                    .setInt(1, 5).setInt(2, 3)
                    .addBatch()
                    .setInt(1, 5).setInt(2, 3)
                    .addBatch()
                    .setInt(1, 4).setInt(2, 3)
                    .addBatch()
                    .setInt(1, 3).setInt(2, 4)
                    .addBatch()
                    .setInt(1, 4).setInt(2, 4)
                    .addBatch()
                    .setInt(1, 4).setInt(2, 4)
                    .addBatch()
                    .setInt(1, 5).setInt(2, 5)
                    .addBatch()
                    .setInt(1, 4).setInt(2, 5)
                    .addBatch()
                    .setInt(1, 4).setInt(2, 5)
                    .addBatch()
                    .execute();

            return trans.batch("insert into dbo.Review(id, movieid, summary, name, fullreview) values (?,?,?,?,?)")
                    .setInt(1, 14).setInt(2, 3)
                    .setString(3, "Muito Fixe").setString(4, "Pedro").setString(5, "Mesmo muito fixe")
                    .addBatch()
                    .setInt(1, 15).setInt(2, 3)
                    .setString(3, "Com Piada").setString(4, "Ze").setString(5, "Até que teve a sua piada")
                    .addBatch()
                    .setInt(1, 16).setInt(2, 3)
                    .setString(3, "Melhor de sempre").setString(4, "To").setString(5, "O resumo diz tudo")
                    .addBatch()
                    .setInt(1, 17).setInt(2, 4)
                    .setString(3, "Muito Fixe").setString(4, "Jaquim").setString(5, "Mesmo muito fixe")
                    .addBatch()
                    .setInt(1, 18).setInt(2, 4)
                    .setString(3, "Brutal").setString(4, "Pedro").setString(5, "Sem palavras")
                    .addBatch()
                    .execute();
        });

        service.transaction((trans) -> {
            trans.batch("insert into dbo.Collections values (?, ?)")
                    .setString(1, "The best").setString(2, "Os melhores de sempre")
                    .addBatch()
                    .setString(1, "Best of 90's").setString(2, "Os melhores dos anos 90")
                    .addBatch()
                    .execute();

            List<Integer> ids = trans.query("select * from collections")
                    .map((rs) -> rs.getInt(1))
                    .collect(toList());

            return trans.batch("insert into dbo.CollectionsMovie(movieid, collectionsid) values (?, ?)")
                    .setInt(1, 3).setInt(2, ids.get(0))
                    .addBatch()
                    .setInt(1, 4).setInt(2, ids.get(0))
                    .addBatch()
                    .setInt(1, 5).setInt(2, ids.get(0))
                    .addBatch()
                    .setInt(1, 3).setInt(2, ids.get(1))
                    .addBatch()
                    .setInt(1, 4).setInt(2, ids.get(1))
                    .addBatch()
                    .execute();
        });
    }
}
