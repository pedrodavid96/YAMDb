package pt.peddavid.yamdb;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import kotlin.Pair;
import pt.peddavid.yamdb.commands.*;
import pt.peddavid.yamdb.htmlformatters.*;
import pt.peddavid.yamdb.libs.command.Command;
import pt.peddavid.yamdb.libs.command.CommandArguments;
import pt.peddavid.yamdb.libs.command.CommandException;
import pt.peddavid.yamdb.libs.command.CommandManager;
import pt.peddavid.yamdb.libs.database.ConnectionService;
import pt.peddavid.yamdb.util.TimeLogger;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;

public class App {

    private static final Logger log = LoggerFactory.getLogger(App.class);

    private static CommandManager manager;

    private static class SingleRunningApp implements Runnable{

        private final String[] args;
        public SingleRunningApp(String[] args){
            this.args = args;
        }

        @Override
        public void run() {
            Pair<Command, CommandArguments> cmdStruct;
            try {
                cmdStruct = TimeLogger.logTimeWithThrow(
                        () -> manager.getCommand(args),
                        CommandManager.class,
                        "Get Command From Infrastructure");
                cmdStruct.getFirst().execute(cmdStruct.getSecond());
            } catch (CommandException e) {
                log.error("Invalid command:\n\t" + e.getMessage());
            }
        }
    }

    private static class LoopRunningApp implements Runnable{
        @Override
        public void run() {
            Scanner scanner = new Scanner(System.in);
            String commandString;
            System.out.print(">");
            while((commandString = scanner.nextLine()) != null){
                try {
                    final String finalCmdString = commandString;
                    final Pair<Command, CommandArguments> cmdStruct = TimeLogger.logTimeWithThrow(
                            () -> manager.getCommand(finalCmdString),
                            CommandManager.class,
                            "Get Command From Infrastructure");
                    TimeLogger.logTimeWithThrow(
                            () -> cmdStruct.getFirst().execute(cmdStruct.getSecond()),
                            Command.class,
                            "Command Execution");
                } catch (CommandException e) {
                    log.error("Invalid Command:\n\t" + e.getMessage());
                }
                System.out.print(">");
            }
        }
    }

    private static class ServerRunningApp implements Runnable{

        private final String commandString;

        public ServerRunningApp(String port){
            commandString = String.format("LISTEN / port=%s", port);
        }

        @Override
        public void run() {
            try {
                Pair<Command, CommandArguments> command = manager.getCommand(commandString);
                command.getFirst().execute(command.getSecond());
            }catch (CommandException e){
                log.error(e.getMessage());
            }
        }
    }

    private static Runnable valueOf(String[] args){
        String port = System.getenv("PORT");
        if(port != null){
            return new ServerRunningApp(port);
        }
        if(args != null && args.length > 0)
            return new SingleRunningApp(args);
        return new LoopRunningApp();
    }

    public static void main(String [] args) throws SQLException, IOException {
        // TODO: Refactor logging
        log.info("Application has started");
        ConnectionService src = TimeLogger.logTimeWithThrow(MSSQLService::new, MSSQLService.class, "Setting up Connection to MSSQL Server");
        TimeLogger.logTime(() -> setUpManager(src), log, "Setting up the Command Infrastructure");
        Runnable app = valueOf(args);
        app.run();
    }

    private static void setUpManager(ConnectionService src) {
        manager = new CommandManager(command -> command.split(" "));
        manager.addCommand(
                "GET /movies",
                new GetCommandImpl<>(
                        new GetAllMoviesCommand(src),
                        new HtmlMovieFormatter.PageFormatter()
                )
        );
        manager.addCommand(
                "GET /movies/{mid}",
                new GetCommandImpl<>(
                        new GetMovieCommand(src),
                        new HtmlMovieFormatter.DetailedMovieFormatter()
                )
        );
        manager.addCommand(
                "GET /movies/{mid}/ratings",
                new GetCommandImpl<>(
                        new GetRatingCommand(src),
                        new HtmlRatingInformationFormatter())
        );
        manager.addCommand(
                "GET /movies/{mid}/reviews",
                new GetCommandImpl<>(
                        new GetReviewsFromMovieCommand(src),
                        new HtmlReviewFormatter.ReviewPageFormatter()
                )
        );
        manager.addCommand(
                "GET /movies/{mid}/reviews/{rid}",
                new GetCommandImpl<>(
                        new GetSpecificReviewFromMovieCommand(src),
                        new HtmlReviewFormatter.SingleReviewFormatter()
                )
        );
        manager.addCommand(
                "GET /tops/ratings/higher/average",
                new GetCommandImpl<>(
                        new GetHigherAverageMovieCommand(src),
                        new HtmlMovieFormatter.MovieWithAverage()
                )
        );
        manager.addCommand(
                "GET /tops/{n}/ratings/higher/average",
                new GetCommandImpl<>(
                        new GetTopHigherAverageMoviesCommand(src),
                        new HtmlMovieFormatter.ListMovieWithAverage()
                )
        );
        manager.addCommand(
                "GET /tops/ratings/lower/average",
                new GetCommandImpl<>(
                        new GetLowestAverageMovieCommand(src),
                        new HtmlMovieFormatter.MovieWithAverage()
                )
        );
        manager.addCommand(
                "GET /tops/{n}/ratings/lower/average",
                new GetCommandImpl<>(
                        new GetTopLowestAverageMovieCommand(src),
                        new HtmlMovieFormatter.ListMovieWithAverage()
                )
        );
        manager.addCommand(
                "GET /tops/reviews/higher/count",
                new GetCommandImpl<>(
                        new GetHigherCountReviewCommand(src),
                        new HtmlMovieFormatter.MovieWithCount()
                )
        );
        manager.addCommand(
                "GET /tops/{n}/reviews/higher/count",
                new GetCommandImpl<>(
                        new GetTopHigherCountReviewsCommand(src),
                        new HtmlMovieFormatter.ListMovieWithCount()
                )
        );

        manager.addCommand(
                "GET /tops/{n}/reviews/lower/count",
                new GetCommandImpl<>(
                        new GetTopLowerCountReviewsCommand(src),
                        new HtmlMovieFormatter.ListMovieWithCount()
                )
        );

        manager.addCommand(
                "GET /collections",
                new GetCommandImpl<>(
                        new GetAllCollectionsCommand(src),
                        new HtmlCollectionFormatter.CollectionPageFormatter()
                )
        );
        manager.addCommand(
                "GET /collections/{cid}",
                new GetCommandImpl<>(
                        new GetCollectionCommand(src),
                        new HtmlCollectionFormatter.SimpleCollectionFormatter()
                )
        );

        manager.addCommand(
                "GET /",
                new GetCommandImpl<>(
                        new DirectoryCommand(
                                new Pair<>("Collections", "/collections/"),
                                new Pair<>("Movies", "/movies/"),
                                new Pair<>("Top", "/tops/ratings/")
                        ),
                        new HtmlDirectoryFormatter()
                )
        );

        manager.addCommand(
                "GET /tops/ratings",
                new GetCommandImpl<>(
                        new DirectoryCommand(
                                new Pair<>("Ratings Higher Average", "/tops/5/ratings/higher/average/"),
                                new Pair<>("Ratings Lower Average", "/tops/5/ratings/lower/average/"),
                                new Pair<>("Reviews Higher Count", "/tops/5/reviews/higher/count/"),
                                new Pair<>("Reviews Lowest Count", "/tops/5/reviews/lower/count/")
                        ),
                        new HtmlDirectoryFormatter(
                                new Pair<>("Movies", "/movies/"),
                                new Pair<>("Home", "/")
                        )
                )
        );

        manager.addCommand("POST /movies", new PostMovieCommand(src));
        manager.addCommand("POST /movies/{mid}/ratings", new PostRatingCommand(src));
        manager.addCommand("POST /movies/{mid}/reviews", new PostReviewCommand(src));
        manager.addCommand("POST /collections", new PostCollectionCommand(src));
        manager.addCommand("POST /collections/{cid}", new PostMovieToCollectionCommand(src));

        manager.addCommand("DELETE /collections/{cid}/movies/{mid}", new DeleteMovieFromCollectionCommand(src));

        manager.addCommand("OPTION /", new OptionCommand(manager));
        manager.addCommand("EXIT /", new ExitCommand());

        manager.addCommand("LISTEN /", new ListenCommand(manager));
    }
}
