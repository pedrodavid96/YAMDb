package pt.peddavid.yamdb.model;

import java.util.List;

public class DetailedMovie {

    private final Movie movie;
    private final List<MovieCollection> collections;
    private final List<Review> reviews;

    public DetailedMovie(Movie movie, List<MovieCollection> collections, List<Review> reviews) {
        this.movie = movie;
        this.collections = collections;
        this.reviews = reviews;
    }

    public Movie getMovie() {
        return movie;
    }

    public List<MovieCollection> getCollections() {
        return collections;
    }

    public List<Review> getReviews(){
        return reviews;
    }

    @Override
    public String toString(){
        return "Detailed Movie: "   + movie.getTitle() +
                " in Collections: " + collections.toString() +
                " has Reviews: "    + reviews.toString();
    }
}
