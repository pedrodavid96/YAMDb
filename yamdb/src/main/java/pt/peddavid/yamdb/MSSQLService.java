package pt.peddavid.yamdb;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import org.slf4j.LoggerFactory;
import pt.peddavid.yamdb.libs.database.ConnectionService;
import pt.peddavid.yamdb.libs.database.connections.BaseConnection;

import java.sql.Connection;
import java.sql.SQLException;


public class MSSQLService implements ConnectionService {

    private final SQLServerDataSource src = new SQLServerDataSource();

    public MSSQLService() throws SQLException {
        try {
            src.setServerName("localhost");
//            src.setPortNumber(1433);
//            src.setDatabaseName(System.getenv("sa"));
            src.setUser("sa");
            src.setPassword("yourStrong(!)Password");
            Connection con = src.getConnection();
            con.close();
        } catch (SQLException e) {
            LoggerFactory.getLogger(MSSQLService.class)
                    .error("Error creating connection to MSSQLServer, " +
                            "check your connection and environment variables\n" + e.getMessage());
            throw e;
        }
    }

    public MSSQLService(String url, String user, String password) throws SQLException {
        try {
            src.setURL(url);
            src.setUser(user);
            src.setPassword(password);
            Connection con = src.getConnection();
            con.close();
        } catch (SQLException e) {
            LoggerFactory.getLogger(MSSQLService.class)
                    .error("Error creating connection to MSSQLServer, " +
                            "check your connection and environment variables\n" + e.getMessage());
            throw e;
        }
    }

    @Override
    public BaseConnection getConnection() throws SQLException {
        return new BaseConnection(src.getConnection());
    }
}
