package pt.peddavid.yamdb.libs.command;

@FunctionalInterface
public interface Command<E> {
    E execute(CommandArguments args) throws CommandException;
}
