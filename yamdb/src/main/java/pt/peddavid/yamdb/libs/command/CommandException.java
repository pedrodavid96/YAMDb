package pt.peddavid.yamdb.libs.command;

public class CommandException extends Exception {

    private final int errorCode;

    public CommandException(int errorCode, String message){
        super(message);
        this.errorCode = errorCode;
    }

    public int getErrorCode(){
        return errorCode;
    }

}
