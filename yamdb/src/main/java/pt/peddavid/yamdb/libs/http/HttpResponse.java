package pt.peddavid.yamdb.libs.http;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Optional;

public class HttpResponse {

    private Charset charSet = Charset.forName("utf-8");
    private String accept = "text/html";

    private final HttpStatusCode statusCode;
    private final Optional<String> content;

    public HttpResponse(HttpStatusCode status){
        this(status, null);
    }

    public HttpResponse(HttpStatusCode status, String content){
        this.statusCode = status;
        this.content = Optional.ofNullable(content);
    }

    public void setCharset(Charset charset){
        this.charSet = charset;
    }

    public void setAccept(String accept){
        this.accept = accept;
    }

    private void sendWithoutBody(HttpServletResponse resp) throws IOException {
        resp.setStatus(statusCode.getCode());
    }

    public void send(HttpServletResponse resp) throws IOException {
        if(!content.isPresent()){
            sendWithoutBody(resp);
            return;
        }

        String cType = String.format("%s; charSet=%s", accept, charSet.name());
        byte[] byteContent = content.get().getBytes(charSet);

        resp.setContentType(cType);
        resp.setContentLength(byteContent.length);
        resp.setStatus(statusCode.getCode());

        OutputStream os = resp.getOutputStream();
        os.write(byteContent);
        os.close();
    }
}
