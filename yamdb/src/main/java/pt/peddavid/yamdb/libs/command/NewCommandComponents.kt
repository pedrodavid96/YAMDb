package pt.peddavid.yamdb.libs.command

import org.eclipse.jetty.http.HttpMethod
import pt.peddavid.commons.Match
import pt.peddavid.commons.MatchResult
import pt.peddavid.commons.Matcher
import pt.peddavid.commons.NoMatch

object MethodMatcher: Matcher<String, HttpMethod> {
    override fun tryMatch(key: String): MatchResult<HttpMethod> {
        // TODO: Missing "non standard" Http Methods (e.g.: PATCH)
        return HttpMethod.fromString(key)?.let(::Match) ?: NoMatch()
    }
}

object PathMatcher: Matcher<String, String> {
    override fun tryMatch(key: String): MatchResult<String> {
        return if (key.isNotEmpty() && key.startsWith("/")) {
            Match(key)
        } else {
            NoMatch()
        }
    }
}

val HeadersMatcher = KeyValuesMatcher(":", "|")
val ParametersMatcher = KeyValuesMatcher("=", "&")

class KeyValuesMatcher(
        private val keyValueSeparator: String,
        private val pairsSeparator: String
) : Matcher<String, Map<String, String>> {
    override fun tryMatch(key: String): MatchResult<Map<String, String>> {
        return Match(key.split(pairsSeparator)
                .map { argument ->
                    val keyValue = argument.split(keyValueSeparator)
                    if (keyValue.size != 2) return NoMatch()
                    val (argKey, argValue) = keyValue
                    Pair(argKey, argValue)
                }
                .toMap())
    }
}
