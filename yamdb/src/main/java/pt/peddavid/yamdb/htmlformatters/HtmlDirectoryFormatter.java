package pt.peddavid.yamdb.htmlformatters;

import kotlin.Pair;
import pt.peddavid.html.HtmlElement;
import pt.peddavid.html.HtmlFactory;
import pt.peddavid.html.HtmlNestedElement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static pt.peddavid.yamdb.htmlformatters.util.HtmlHyperLink.hyperLink;

public class HtmlDirectoryFormatter implements HtmlFormatter<List<Pair<String, String>>> {

    private final List<Pair<String, String>> additionalButtons;

    public HtmlDirectoryFormatter(){
        additionalButtons = new ArrayList<>();
    }

    @SafeVarargs
    public HtmlDirectoryFormatter(Pair<String, String>... additionalButtons){
        this.additionalButtons = Arrays.asList(additionalButtons);
    }

    @Override
    public HtmlElement supplyHtml(List<Pair<String, String>> source) {
        HtmlNestedElement body = HtmlFactory.body();
        HtmlNestedElement table = HtmlFactory.table();
        for (Pair<String, String> pair : source){
            table.with(HtmlFactory.tr().with(
                    HtmlFactory.td().with(hyperLink(
                            pair.getFirst(),
                            pair.getSecond())
                    ))
            );
        }
        body.with(table);
        for (Pair<String, String> button : additionalButtons){
            body.with(HtmlFactory.h3().with(hyperLink(button.getFirst(), button.getSecond())));
        }
        return HtmlFactory.html().with(body);
    }
}
