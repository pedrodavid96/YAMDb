package pt.peddavid.yamdb;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LogTests {
    private Logger logger = LoggerFactory.getLogger(LogTests.class);

    @Test
    public void testLoggerImplementationIsNotNOP() {
        String loggerNam = logger.getName();
        assertEquals(LogTests.class.getName(), loggerNam);
    }
}
