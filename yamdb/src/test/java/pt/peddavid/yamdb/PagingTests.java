package pt.peddavid.yamdb;

import org.junit.jupiter.api.Test;
import pt.peddavid.yamdb.model.Movie;
import pt.peddavid.yamdb.util.Page;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PagingTests {

    private List<Movie> getMovies(){
        List<Movie> movies = new ArrayList<>();
        movies.add(new Movie(1, "Hello World", 2016, LocalDate.now()));
        movies.add(new Movie(2, "Hello World 2", 2015, LocalDate.now()));
        return movies;
    }

    @Test
    public void testNullSkipAndTop() {
        List<Movie> movies = getMovies();
        assertThrows(IllegalArgumentException.class, () -> new Page<>(null, null, movies));
    }

    @Test
    public void testNullSrc() {
        assertThrows(IllegalArgumentException.class, () -> new Page<>(null, null, null));
    }

    @Test
    public void pageWithSameSizeAsList() {
        Page<Movie> movies = new Page<>(Optional.empty(), Optional.of(2), getMovies());
        assertFalse(movies.hasPrevious());
        assertFalse(movies.hasNext());
        assertEquals(movies.getQuery(), "top=2");
    }

    @Test
    public void pageWithSameSizeAsListThenAddMember() {
        Page<Movie> movies = new Page<>(Optional.empty(), Optional.of(2), getMovies());
        assertFalse(movies.hasPrevious());
        assertFalse(movies.hasNext());
        assertEquals(movies.getQuery(), "top=2");
        movies.add(new Movie(1, "Going trough top", 2017, LocalDate.now()));
        assertTrue(movies.hasNext());
    }

    @Test
    public void pageWithSameSizeAsListThenRemoveMember() {
        Page<Movie> movies = new Page<>(Optional.empty(), Optional.of(1), getMovies());
        assertFalse(movies.hasPrevious());
        assertTrue(movies.hasNext());
        assertEquals(movies.getQuery(), "top=1");
        movies.getList().remove(0);
        assertFalse(movies.hasNext());
    }

    @Test
    public void skipIsLessThan0() {
        assertThrows(IllegalArgumentException.class, () -> new Page<>(Optional.of(-1), Optional.of(0)));
    }

    @Test
    public void topIsLessThan0() {
        assertThrows(IllegalArgumentException.class, () -> new Page<>(Optional.of(0), Optional.of(-1)));
    }

    @Test
    public void topIsEmpty() {
        Page<Movie> movies = new Page<>(Optional.of(5), Optional.empty(), getMovies());
        assertTrue(movies.hasPrevious());
        assertFalse(movies.hasNext());
        assertEquals(movies.getQuery(), "skip=5");
    }

    @Test
    public void getQueryOfSkipAndTop() {
        Page<Movie> movies = new Page<>(Optional.of(5), Optional.of(7), getMovies());
        assertEquals(movies.getQuery(), "skip=5&top=7");
    }

}
