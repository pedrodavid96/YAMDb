package pt.peddavid.yamdb;

import org.junit.jupiter.api.Test;

import javax.servlet.http.HttpServlet;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ServletTest {
    @Test
    public void newServletIsNotNull() {
        HttpServlet servlet = new HttpServlet() {};
        assertNotNull(servlet);
    }
}
