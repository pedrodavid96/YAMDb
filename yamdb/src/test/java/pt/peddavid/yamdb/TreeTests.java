package pt.peddavid.yamdb;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import pt.peddavid.commons.NTree;
import pt.peddavid.commons.SortedNTreeMap;
import pt.peddavid.yamdb.libs.command.Command;
import pt.peddavid.yamdb.libs.command.CommandComponent;
import pt.peddavid.yamdb.libs.command.CommandComponents;
import pt.peddavid.yamdb.libs.command.CommandException;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

public class TreeTests {

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 10, 100})
    public void trueQueryReturnsMatchingArity(int arity) {
        NTree<String, String> tree = new NTree<>((key, userKey) -> true);
        tree.add(new String[arity]);

        assertNotNull(tree.get(new String[arity]));
        assertNull(tree.get(new String[arity + 1]));
        assertNull(tree.get(new String[arity + 2]));
        assertNull(tree.get(new String[arity + 3]));
    }

    @ParameterizedTest
    @ValueSource(strings = {"test", "path"})
    public void singlePathWithEqualsPredicateNotNullIfSameObject(String path) {
        NTree<String, String> tree = new NTree<>(String::equals);
        tree.add(path);
        assertNotNull(tree.get(path));
    }

    @Test
    public void NTreeBackTracking(){
        NTree<CommandComponent, String> cmp = new NTree<>(CommandComponent::test);
        cmp.add(CommandComponents.Component.Method);
        cmp.add(CommandComponents.Component.Method, CommandComponents.Component.Path);
        cmp.add(CommandComponents.Component.Method, CommandComponents.Component.Method);
        cmp.add((string) -> true, CommandComponents.Component.Method);

        assertNotNull(cmp.get("PUT", "/mundo"));
        assertNotNull(cmp.get("CONNECT"));
        assertNotNull(cmp.get("ola" , "GET"));
        assertNotNull(cmp.get("POST" , "GET"));
        assertNull(cmp.get("PUT", "mundo"));
        assertNull(cmp.get("ola", "/mundo"));
        assertNull(cmp.get("ola"));
    }


    @Test
    public void PathToCommandTest() throws CommandException {
        SortedNTreeMap<String, Command> pathTree = new SortedNTreeMap<>();
        String[] commands = new String[]{
                "/test/one",
                "/test/two",
                "/this/test/three",
                "/we/are/at/four",
                "/we/are/at/five",
                "/this/test/six"
        };
        final int[] commandsResult = new int[commands.length];
        for(int i = 0; i < commands.length; ++i) {
            final int finalI = i;
            pathTree.add((s) -> {
                commandsResult[finalI] = finalI;
                return null;
            }, commands[i].split("/"));
        }
        for(int i = 0; i < commands.length; ++i) {
            Command cmd = pathTree.get(commands[i].split("/"));
            cmd.execute(null);
            assertEquals(i, commandsResult[i]);
        }
    }

    @Test
    public void PathWithBackUpForVariables(){
        final Map<String, String> vars = new HashMap<>();
        SortedNTreeMap<String, Command> pathTree = new SortedNTreeMap<>(
                (tree, obj) -> {
                    String lastValue = tree.get(tree.size() - 1);
                    if(lastValue.startsWith("{") && lastValue.endsWith("}")){
                        vars.put(lastValue, obj);
                    }
                    return tree.size() - 1;
                }
        );
        String[] commands = new String[]{
                "test/{one}",
                "test/two",
                "this/{test}/three",
                "we/are/at/four",
                "we/are/at/five",
                "this/test/six"
        };
        for (String command : commands) {
            pathTree.add((s) -> null, command.split("/"));
        }
        assertNotNull(pathTree.get("test", "2"));
        assertEquals(vars.get("{one}"), "2");
        //Note: Care, even tough path was not found it still captured the value
        // since it is added at evaluation time in backUpPlan
        assertNull(pathTree.get("this", "3"));
        assertEquals(vars.get("{test}"), "3");
        assertNotNull(pathTree.get("this", "4", "three"));
        assertEquals(vars.get("{test}"), "4");
    }
}
