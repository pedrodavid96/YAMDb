package pt.peddavid.commons

internal class ProxyNode<K, V>(val key: K) {
    val tree: ArrayList<ProxyNode<K, V>> = ArrayList()
    var value: V? = null

    fun prefixAllPaths(prev: String): String {
        var result = ""
        val prefix = "$prev${this}/"
        if (value != null) {
            result += "$prefix -> ${value.toString()}\n"
        }
        for (node in tree) {
            result += node.prefixAllPaths(prefix)
        }
        return result
    }

    override fun toString(): String {
        return key.toString()
    }
}
