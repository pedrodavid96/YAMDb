package pt.peddavid.commons;

import java.util.Optional;
import java.util.function.BiPredicate;

public class NTree<K, UserKey> extends NTreeMap<K, UserKey, Optional<Object>> {
    public NTree(BiPredicate<K, UserKey> predicate) {
        super(predicate);
    }

    public void add(K... list) {
        super.add(Optional.empty(), list);
    }
}
