package pt.peddavid.commons

class TreeMatcher<UserK, R : Any> {
    private val matchersToAggregators: MutableList<Triple<MatcherToAggregator<UserK, *, R>, TreeMatcher<UserK, R>, Boolean>> = ArrayList()

    operator fun get(vararg keys: UserK): MatchResult<R> {
        return get(listOf(*keys), currentResult = null)
    }

    private fun get(keys: List<UserK>, currentResult: R?): MatchResult<R> {
        return matchersToAggregators
                .map { node ->
                    val matcherToAggregator = node.first
                    val tryMatch = matcherToAggregator.matcher.tryMatch(keys[0])
                    if (tryMatch is Match) {
                        @Suppress("UNCHECKED_CAST")
                        val safeAggregator = matcherToAggregator.aggregator
                                as? (R?, Any) -> R ?: throw SafeCastException()
                        val result = safeAggregator(currentResult, tryMatch.result)
                        val remainingKeys = keys.drop(1)
                        if (remainingKeys.isEmpty()) {
                            if (node.third) {
                                Match(result)
                            } else {
                                NoMatch()
                            }
                        } else {
                            node.second.get(remainingKeys, currentResult = result)
                        }
                    } else {
                        NoMatch()
                    }
                }
                .firstOrNull { it is Match<*> } ?: NoMatch()
    }

    fun add(vararg matchersToAggregators: MatcherToAggregator<UserK, *, R>) {
        add(listOf(*matchersToAggregators))
    }

    fun add(matchersToAggregators: List<MatcherToAggregator<UserK, *, R>>) {
        if (matchersToAggregators.isEmpty()) {
            return
        }

        val innerTree: TreeMatcher<UserK, R>
        val find = this.matchersToAggregators.find { mtA -> mtA.first == matchersToAggregators[0] }
        if (find != null) {
            innerTree = find.second
        } else {
            innerTree = TreeMatcher()
            this.matchersToAggregators.add(Triple(matchersToAggregators[0], innerTree, matchersToAggregators.size == 1))
        }
        innerTree.add(matchersToAggregators.drop(1))
    }
}

class SafeCastException : IllegalStateException("""Something is really wrong.
    |Due to the nature of the * projection aggregator is resolved to a `PartialR` as `Nothing`.
    |This cast is safe since:
    |* `tryMatch.result` isn't `Nothing` since that can only be returned by throwing,
    |  which would skip this cast.
    |* `Matcher` and `MatchResult` interface guarantees it is `PartialR : Any` plus
    |* Interface to add matchers + aggregators guarantees matching types"""
        .trimMargin())

// PartialR needs to extend `Any` (instead of the implicit `Any?`) to be sure that the resulting `PartialR`
//  isn't nullable and aggregator can be called.
data class MatcherToAggregator<K, PartialR : Any, FinalR>(
        val matcher: Matcher<K, PartialR>,
        val aggregator: (FinalR?, PartialR) -> FinalR
)

// Any has to be pushed upstream for `MatcherToAggregator`
interface Matcher<K, PartialR : Any> {
    fun tryMatch(key: K): MatchResult<PartialR>

    infix fun <FinalR> then(aggregator: (FinalR?, PartialR) -> FinalR) = MatcherToAggregator(this, aggregator)
}

sealed class MatchResult<R : Any>
class Match<R : Any>(val result: R) : MatchResult<R>()
class NoMatch<R : Any> : MatchResult<R>()
