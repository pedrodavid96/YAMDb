package pt.peddavid.html;

public class HtmlEmptyElement implements HtmlElement {

    private final HtmlTag tag;

    public HtmlEmptyElement(String tag){
        this.tag = new HtmlTag(tag);
    }

    public HtmlEmptyElement setAttribute(String name, String value){
        tag.setAttribute(name, value);
        return this;
    }

    @Override
    public String toHtml() {
        return tag.startTag();
    }

    @Override
    public String toString() {
        return toHtml();
    }
}
