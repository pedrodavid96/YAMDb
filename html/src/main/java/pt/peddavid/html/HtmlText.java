package pt.peddavid.html;

public class HtmlText implements HtmlElement {

    private final String text;

    public HtmlText(String text){
        this.text = text;
    }

    @Override
    public String toHtml() {
        return text;
    }

    @Override
    public String toString() {
        return toHtml();
    }

    @Override
    public String toHtmlDocument(){
        throw new UnsupportedOperationException("Can't covert single text to html document");
    }
}
