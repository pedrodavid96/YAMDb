package pt.peddavid.html;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * Created by HP on 20/04/2016.
 */
public class HtmlTests {

    @Test
    public void TableNewHtmlTest(){
        String expected = "<!DOCTYPE html><html><table border=\"1\" style=\"width:50%\">" +
                "<tr><th>Test1</th><th>Test2</th><th>Test3</th></tr>" +
                "<tr><td>Test4</td><td>Test5</td><td>Test6</td></tr>" +
                "</table></html>";
        HtmlElement html = HtmlFactory.html().with(
                HtmlFactory.table()
                        .setAttribute("border", "1")
                        .setAttribute("style", "width:50%")
                        .with(
                                HtmlFactory.tr().with(
                                        HtmlFactory.th("Test1"),
                                        HtmlFactory.th("Test2"),
                                        HtmlFactory.th("Test3")
                                ),
                                HtmlFactory.tr().with(
                                        HtmlFactory.td("Test4"),
                                        HtmlFactory.td("Test5"),
                                        HtmlFactory.td("Test6")
                                )
                        )
        );
        String actual = html.toHtmlDocument();
        assertEquals(expected, actual);
    }
}
